use std::env;
use std::io::{copy, BufRead, BufReader, Error, Read, Write};
use std::net::TcpListener;
use std::net::{Shutdown, TcpStream};
use std::string;
use std::thread;
use std::time::Duration;

fn copyStream(w: &mut Write, r: &mut Read) {
    copy(r, w);
}

fn handleStream(mut sockClient: TcpStream) {
    match TcpStream::connect(std::env::args().nth(2).unwrap()) {
        Ok(mut tcp) => {
            let mut buffer = BufReader::new(&sockClient);
            while (true) {
                let mut received_message_buf = [0].repeat(2048);
                match buffer.read(&mut received_message_buf) {
                    Ok(bufN) => {
                        println!("RSI leido {}", bufN);
                    }
                    Err(err) => {
                        println!("Error mientras se leia: {:?}", err);
                        return;
                    }
                }
                if buffer.buffer().is_empty() {
                    break;
                }
            }
            sockClient.write(b"HTTP/1.1 101 Web Socket Protocol Handshake\r\nConnection: Upgrade\r\nUpgrade: websocket\r\n\r\nHTTP/1.1 200 Connection Established\r\n\r\n");
            sockClient.flush();
            let mut tcp1 = tcp.try_clone().unwrap();
            let mut sockClient1 = sockClient.try_clone().unwrap();
            thread::spawn(move || {
                copyStream(&mut tcp, &mut sockClient);
                tcp.shutdown(Shutdown::Both);
                sockClient.shutdown(Shutdown::Both);
            });
            copyStream(&mut sockClient1, &mut tcp1);
            tcp1.shutdown(Shutdown::Both);
            sockClient1.shutdown(Shutdown::Both);
        }
        Err(e) => {
            println!("Hubo un errro al conectar {:?}", e);
            sockClient.shutdown(Shutdown::Both);
        }
    }
}

fn showCredits() {
    println!("Este binario fue construido por @rj789 y parte del team de newtoolsworks, creditos del modo de conexion ?????")
}

// ./binario 0.0.0.0:80 127.0.0.1:22
fn main() {
    showCredits();
    if (env::args().len() < 3) {
        println!("Porfavor configura el servidor de escucha y el target del format IP:PUERTO");
        return;
    }
    println!("Simple Server WSRequest HTTP v1.0 Open Source Edition");
    println!("Esuchando en: {}", env::args().nth(1).unwrap());
    println!("Tunnel hacia: {}", env::args().nth(2).unwrap());
    let listener = TcpListener::bind(env::args().nth(1).unwrap()).unwrap();
    while (true) {
        match listener.accept() {
            Ok((sockClient, addr)) => {
                thread::spawn(|| {
                    handleStream(sockClient);
                });
            }
            Err(e) => println!("couldn't get client: {:?}", e),
        }
    }
}
