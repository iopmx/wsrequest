# Descripcion ES
**Este binario se instala en el lado del servidor** te permite emular la inyeccion del protocolo WebSocket y reenviarlo a otra aplicacion [SSH] 
Fue creador por @rj789 y parte del team de newtoolsworks, creditos a esta conexion a su creador solo implementamo
la forma de conexion ya que recibimos correos solicitando esto, al igual nos gusta escribir codigo

Puedes descargar el binario  directamente desde el apartado descargas.

# Descripcion EN
**This binary is installed on the server side** it allows you to emulate the injection of the WebSocket protocol and forward it to another application [SSH]
It was created by @ rj789 and part of the newtoolsworks team, credits to this connection to its creator I only implement
the connection form since we receive emails requesting this, just as we like to write code

You can download this binary directly from side downloads


## Build from source
Install Rust

Install dependency from cargo (Package Gestor)


## build
cargo build 

cargo run

the executable is found in the release directory

